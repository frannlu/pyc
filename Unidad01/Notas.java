package unidad01;

public class Notas {

	public static void main(String[] args) {

		/* Estructura condicional

      Crea un programa que pida al usuario una nota entre 0 y 10. En función de
      la nota introducida se debe mostrar un mensaje:

      Si la nota es 0: "Muy Deficiente"
      Si la nota es 3: "Insuficiente"
      Si la nota es 5: "Suficiente"
      Si la nota es 6: "Bien"
      Si la nota es 8: "Notable"
      Si la nota es 9: "Sobresaliente"
      Si la nota es 10: "Matricula de Honor"

      Ampliación: Intervalos con valores decimales [0-0.99][1-4.99] ...

    */
