Práctica 1 (Evaluable)
===========

1. Escribir un programa que pregunte al usuario por el número de horas trabajadas y el coste por hora. Después debe mostrar por pantalla la paga que le corresponde.

2. Escribir un programa que lea un entero positivo, n , introducido por el usuario y después muestre en pantalla la suma de todos los enteros desde 1 hasta n. La suma de los primeros enteros positivos puede ser calculada de la siguiente forma:  
    suma = (n * (n+1)) / 2

3. Escribir un programa que pida al usuario su peso (en kg) y estatura (en metros), calcule el índice de masa corporal y lo almacene en una variable, y muestre por pantalla la frase Tu índice de masa corporal es <imc> donde <imc> es el índice de masa corporal calculado.

4. Escribir un programa que pida al usuario dos números enteros y muestre por pantalla: n entre m da un cociente c y un resto r donde n y m son los números introducidos por el usuario, y c y r son el cociente y el resto de la división entera respectivamente.

5. Escribir un programa que pregunte al usuario una cantidad a invertir, el interés anual y el número de años, y muestre por pantalla el capital obtenido en la inversión.

6. Una juguetería tiene mucho éxito en dos de sus productos: payasos y muñecas. Suele hacer venta por correo y la empresa de logística les cobra por peso de cada paquete así que deben calcular el peso de los payasos y muñecas que saldrán en cada paquete a demanda. Cada payaso pesa 112 g y cada muñeca 75 g. Escribir un programa que lea el número de payasos y muñecas vendidos en el último pedido y calcule el peso total del paquete que será enviado.

7. Imagina que acabas de abrir una nueva cuenta de ahorros que te ofrece el 4% de interés al año. Estos ahorros debido a intereses, que no se cobran hasta finales de año, se te añaden al balance final de tu cuenta de ahorros. Escribir un programa que comience leyendo la cantidad de dinero depositada en la cuenta de ahorros, introducida por el usuario. Después el programa debe calcular y mostrar por pantalla la cantidad de ahorros tras el primer, segundo y tercer años.

8. Una panadería vende barras de pan a 3.49€ cada una. El pan que no es el día tiene un descuento del 60%. Escribir un programa que comience leyendo el número de barras vendidas que no son del día. Después el programa debe mostrar el precio habitual de una barra de pan, el descuento que se le hace por no ser fresca y el coste final total.

9. Escribir un programa que almacene la cadena de caracteres contraseña en una variable, pregunte al usuario por la contraseña e imprima por pantalla si la contraseña introducida por el usuario coincide con la guardada en la variable.

10. Escribir un programa que pida al usuario un número entero y muestre por pantalla si es par o impar.

11. Para tributar un determinado impuesto se debe ser mayor de 16 años y tener unos ingresos iguales o superiores a 1000 € mensuales. Escribir un programa que pregunte al usuario su edad y sus ingresos mensuales y muestre por pantalla si el usuario tiene que tributar o no.

12. Los alumnos de un curso se han dividido en dos grupos A y B de acuerdo al sexo y el nombre. El grupo A esta formado por las mujeres con un nombre anterior a la M y los hombres con un nombre posterior a la N y el grupo B por el resto. Escribir un programa que pregunte al usuario su nombre y sexo, y muestre por pantalla el grupo que le corresponde.

13. Los tres lados a, b y c de un triángulo deben satisfacer la [desigualdad triangular](https://es.wikipedia.org/wiki/Desigualdad_triangular): cada uno de los lados no puede ser más largo que la suma de los otros dos.
Escriba un programa que reciba como entrada los tres lados de un triángulo, e indique:

    * si acaso el triángulo es inválido; y
    * si no lo es, qué tipo de triángulo es (isoceles, escaleno ...).

14. Escribe un programa que pida al usuario los siguientes datos: días, horas y minutos. Y le conteste con la cantidad de segundos totales que son esos datos.

15. Pida por teclado una hora en tres variables: horas, minutos y segundos (datos enteros). Muestre por pantalla: "HORA CORRECTA", en el caso de que la hora sea válida. "HORA INCORRECTA", en el caso de que la hora no sea válida.

16. Escribe un programa que muestre por pantalla si un número entero introducido por el usuario es primo o no.

17. Lee un número por teclado que pida el precio de un producto (puede tener decimales) y calcule el precio final con IVA. El IVA sera del 21%.

18. Escribe un programa que declare una variable C de tipo entero y asígnale un valor. A continuación muestra un mensaje indicando si el valor de C es positivo o negativo, si es par o impar, si es múltiplo de 5, si es múltiplo de 10 y si es mayor o menor que 100. Consideraremos el 0 como positivo.

19. Elabora un programa que solicite la medida en Pies y realice la conversión a pulgadas, yardas, cm y metros. Toma en cuenta que un pie tiene 12 pulgadas y una pulgada equivale a 2.54 cm.

20. Escribe un programa que calcule el área de una pirámide regular. El usuario debe introducir los valores para el número de lados de la base, la longitud de los lados, apotema de la base y apotema de la pirámide. Antes de mostrar el resultado el programa debe mostrar un dibujo (ASCII art) de una pirámide regular.
